这个项目演示如何存储和检索数据，具体到每个用户和Windows商店应用程序使用Windows运行时应用程序数据API。

应用数据包括会话状态，用户的喜好，和其他设置，当应用程序运行被数据会被创建，读取，更新，删除。。

源码来源：http://code.msdn.microsoft.com/windowsapps/ApplicationData-sample-fb043eb2

首先是效果图，然后是几个核心代码，最后是源码